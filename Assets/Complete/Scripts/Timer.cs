﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Completed;

public class Timer : MonoBehaviour {

	public Player PlayerGO;
	public Text TimerText;

	public int StartTime = 10;
	private float _currentTime;

	// Use this for initialization
	void Start () {
		_currentTime = StartTime;
		TimerText = GetComponent<Text> ();
		Completed.BoardManager.NewLevelEvent.AddListener (() => {
			_currentTime = StartTime;
		});
	}
	
	// Update is called once per frame
	void Update () {
		if (!Completed.GameManager.doingSetup) {
			if (_currentTime < 0) {
				PlayerGO.food = 0;
				PlayerGO.CheckIfGameOver ();
				_currentTime = 0;
				TimerText.text = "Timer:" + 0;
			} else {
				_currentTime -= Time.deltaTime;
				TimerText.text = "Timer:" + (int)_currentTime;
			}
		}
	}
}
